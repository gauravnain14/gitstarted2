import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Hello! What is your name?");
        String name = scanner.next();
        int random = (int) (Math.random() * 20);
        boolean exit = false;
        guessTheNumber(scanner, 0, name, random, exit);
        System.out.print("\nPROGRAM TERMINATED! BETTER LUCK NEXT TIME!");
    }

    public static void guessTheNumber(Scanner scanner, int count, String name, int random, boolean exit) {
        System.out.println("Well, " + name + ", I am thinking of a number between 1 and 20.\nTake a guess.");
        for (int i = 0; i < 6 && !exit; i++) {
            int value = Integer.parseInt(scanner.next());
            if (value < random) {
                System.out.println("Your guess is too low.\nTake a guess.");
            } else if (value > random) {
                System.out.println("Your guess is too high.\nTake a guess.");
            } else {
                System.out.println("Good job, " + name + "! You guessed my number in " + i + " guesses!\n" +
                        "Would you like to play again? (y or n)");
                String playAgain = scanner.next();
                if (playAgain.equals("y") || playAgain.equals("Y")) {
                    random = (int) (Math.floor(Math.random() * 20)+1);
                    guessTheNumber(scanner, 0, name, random, exit);
                } else
                    exit = true;
                break;
            }
        }

    }
}